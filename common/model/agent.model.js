
module.exports = (sequelize, type) => {
    return sequelize.define('DCA_AGENT', {
        AGENT_ID : {
          type : type.INTEGER,
          primaryKey : true,
          defaultValue: "nextval('DCA_ORDER_SEQ')"
          //autoIncrement : true
        },
        ADDRESS_ARABIC : type.STRING,
        ADDRESS_ENGLISH : type.STRING,
        ENTITY_SIZE : type.INTEGER,
        ENTITY_SPACE : type.INTEGER,
        LOGIN_ID : type.INTEGER,
        MAIN_AGENT : type.INTEGER,
        NAME_ARABIC : type.STRING,
        NAME_ENGLISH : type.STRING,
        OOM_AGENT_ID : type.STRING,
        SFID : type.STRING,
        STATUS_CHANGE_COMMENT : type.STRING ,
        STATUS_ID : type.INTEGER,
        STOCK_MODE : type.INTEGER,
        TKA_ID : type.INTEGER,
        VFE_ACCOUNT_MANAGER_ID : type.INTEGER,
        VFE_SALES_EXECUTIVE_ID : type.INTEGER,
        CITY_ID : type.INTEGER ,
        REGION_ID : type.INTEGER,
        ZONE_ID : type.INTEGER 
    
    },    
    {
        timestamps : false,
        tableName : 'DCA_ORDER'      
    })
}