module.exports =Object.freeze( {
       
        dcaPool: {
          user: process.env.HR_USER || 'dca',
          password: process.env.HR_PASSWORD || 'dca',
          connectString: process.env.HR_CONNECTIONSTRING || '10.230.86.45:1525/DCATEST1',
          poolMin: 5,
          poolMax: 10,
          poolIncrement: 1
        }
      ,asmaaPool: {
          user: process.env.HR_USER || 'Asmaa',
          password: process.env.HR_PASSWORD || 'asmaa',
          connectString: process.env.HR_CONNECTIONSTRING || 'localhost/orcl.168.10.28',
          poolMin: 5,
          poolMax: 10,
          poolIncrement: 1
        }
      });

