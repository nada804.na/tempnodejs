const orderService=require('../../order/service/order.service');
const logger=require('../../common/util/logger.util');
const BusinessConstants= require('../../common/constant/business.constant')

exports.getRejectedOrders = (req, res, next) => {
   
    logger.info(`Order.controller : getRejectedOrders : Started `);
    res.status(200).send('hello ya nada ^_^')
   // req.query.statusId = BusinessConstants.ORDER_STATUS_REJECTED;
    /* orderService.getOrdersByStatusAndAgentId(req.query)
        .then((result)=>{
            logger.info(`Order.controller : getApprovedOrders : End sucessfully `);
            res.setHeader("Content-Type", "application/json"); 
            result={
                    code:BusinessConstants.NO_ERRORS_CODE,
                    key:BusinessConstants.NO_ERRORS_KEY,
                    response:result
                }
            res.status(BusinessConstants.NO_ERRORS_CODE).send(JSON.stringify(result));
        })
        .catch(error =>{
            if(error.message != undefined){
                logger.error(`Order.controller : getRejectedOrders : error :: ${error}` );
             }
            next(error);
               //res.status(400).send(error)
         });
     */    
};

exports.getPendingOrders = (req, res, next) => {
    
    logger.info(`Order.controller : getPendingOrders : Started `);
    req.query.statusId= BusinessConstants.ORDER_STATUS_PENDING;
    orderService.getOrdersByStatusAndAgentId(req.query)
        .then((result)=>{
            logger.info(`Order.controller : getApprovedOrders : End sucessfully `);
            res.setHeader("Content-Type", "application/json"); 
            result={
                    code:BusinessConstants.NO_ERRORS_CODE,
                    key:BusinessConstants.NO_ERRORS_KEY,
                    response:result
                }
            res.status(BusinessConstants.NO_ERRORS_CODE).send(JSON.stringify(result));
        })
        .catch(error =>{
            if(error.message != undefined){
                logger.error(`Order.controller : getPendingOrders : error :: ${error}` );
             }
            next(error);
               //res.status(400).send(error)
         });
        
};

exports.getDraftOrders = (req, res, next) => {
    
    logger.info(`Order.controller : getDraftOrders : Started `);
    req.query.statusId=BusinessConstants.ORDER_STATUS_DRAFT
    orderService.getOrdersByStatusAndAgentId(req.query)
        .then((result)=>{
            logger.info(`Order.controller : getApprovedOrders : End sucessfully `);
            res.setHeader("Content-Type", "application/json"); 
            result={
                    code:BusinessConstants.NO_ERRORS_CODE,
                    key:BusinessConstants.NO_ERRORS_KEY,
                    response:result
                }
            res.status(BusinessConstants.NO_ERRORS_CODE).send(JSON.stringify(result));
        })
        .catch(error =>{
            if(error.message != undefined){
                logger.error(`Order.controller : getDraftOrders : error :: ${error}` );
             }
            next(error);
               //res.status(400).send(error)
         });
        
};

exports.getApprovedOrders = (req, res, next) => {
    
    logger.info(`Order.controller : getApprovedOrders : Started  `);
    req.query.statusId=`${BusinessConstants.ORDER_STATUS_APPROVED1},
                        ${BusinessConstants.ORDER_STATUS_APPROVED2},
                        ${BusinessConstants.ORDER_STATUS_APPROVED3},
                        ${BusinessConstants.ORDER_STATUS_APPROVED4}`
    req.query.statusId= req.query.statusId .replace(/ /g,"").replace(/\n/g,"");
    orderService.getOrdersByStatusAndAgentId(req.query)
        .then((result)=>{
            logger.info(`Order.controller : getApprovedOrders : End sucessfully `);
            res.setHeader("Content-Type", "application/json"); 
            result={
                    code:BusinessConstants.NO_ERRORS_CODE,
                    key:BusinessConstants.NO_ERRORS_KEY,
                    response:result
                }
            res.status(BusinessConstants.NO_ERRORS_CODE).send(JSON.stringify(result));
        })
        .catch(error =>{
            next(error);
         });
        
};

exports.openOrder = (req, res, next) => {
  try{  
    logger.info(`Order.controller : openOrder : Started `);
    
    logger.info(`Order.service : openOrder : Started`);
    if(req.query.orderNumber==undefined){
        err= new Error(`${BusinessConstants.BAD_REQUEST_KEY},orderNumber is missing `);
        //err.code = BusinessConstants.BAD_REQUEST_CODE;
       // err.key= BusinessConstants.BAD_REQUEST_KEY;
        throw err;
      }
    orderService.openOrder(req.query)
        .then((result)=>{
            logger.info(`Order.controller : getApprovedOrders : End sucessfully `);
            res.setHeader("Content-Type", "application/json"); 
            result={
                    code:BusinessConstants.NO_ERRORS_CODE,
                    key:BusinessConstants.NO_ERRORS_KEY,
                    response:result
                }
            res.status(BusinessConstants.NO_ERRORS_CODE).send(JSON.stringify(result));
        })
        .catch(error =>{
            if(error.message != undefined){
                logger.error(`Order.controller : openOrder : error :: ${error}` );
             }
            next(error);
               //res.status(400).send(error)
         });
        } catch(error ){
            error.operationName='Order.controller ';
            error.serviceName='openOrder';
            error.code=BusinessConstants.BAD_REQUEST_CODE;
            error.key=BusinessConstants.BAD_REQUEST_KEY;
            next(error);
               //res.status(400).send(error)
        };
};




