const orderDao = require('../../order/dao/order.dao');
const logger=require('../../common/util/logger.util');
const BusinessConstants= require('../../common/constant/business.constant')


exports.getOrdersByStatusAndAgentId = (query) => {
  return new Promise(async (resolve, reject) => {
  try{
      logger.info(`Order.service : getOrdersByStatusAndAgentId : Started`);
      if(query.sort!=undefined){
       
        if(query.statusId == BusinessConstants.ORDER_STATUS_REJECTED){
          query= mapRejectedPageSorting(query);
        }else if(query.statusId.includes(`,${BusinessConstants.ORDER_STATUS_APPROVED4}`)){
          query= mapApprovedPageSorting(query);
        }else{
          /* cas pending,draft orders */
          query= mapPageSorting(query);
        }
        }else{
        query.property='ORDER_DATE';
        query.direction='DESC';
      }

    let orders= await orderDao.findByStatusIdAndAgentId(query);
    logger.info(`Order.service : getOrdersByStatusAndAgentId : Ended`);
    resolve(orders); 
  }
  catch(err){
    if(err.operationName ==undefined){
        err.operationName='Order.service';
        err.serviceName='getOrdersByStatusAndAgentId';
     } 
     reject(err)     
  }  
});}

function mapApprovedPageSorting( query ) {
 
  logger.info(`Order.service : mapApprovedPageSorting : Started`);
   sort=query.sort.split(",");
      /* property*/
      if(sort[0]=='0'){
        query.property='o.ORDER_NUMBER';
      }else if(sort[0]=='1'){
        query.property='o.ORDER_DATE';
      }else if(sort[0]=='2') {
        if(query.agentId!= undefined){
          query.property='a.NAME_ARABIC';
        }else{
            query.property='a.NAME_ENGLISH';
        }
      }else if(sort[0]=='3'){
        query.property='l.NAME_ARABIC';
      }else{
        query.property='o.ORDER_DATE';
      }
  /* *direction*/
if(sort[1] !=undefined && (sort[1].toUpperCase()=='DESC' || sort[1].toUpperCase()=='ASC')){
    query.direction=sort[1].toUpperCase();  
  }else{
      query.direction='DESC';
  }
  logger.info(`Order.service : mapApprovedPageSorting : Ended`);
  return query;
};
function mapRejectedPageSorting( query ) {

  logger.info(`Order.service : mapRejectedPageSorting : Started`);
  sort=query.sort.split(",");
    /* property*/
    if(sort[0]=='0'){
      query.property='o.ORDER_NUMBER';
    }else if(sort[0]=='1'){
      query.property='o.ORDER_DATE';
    }else if(sort[0]=='2') {
      if(query.agentId!= undefined){
        query.property='a.NAME_ARABIC';
      }else{
          query.property='a.NAME_ENGLISH';
      }
    }else if(sort[0]=='3'){
      query.property='o.REJECTION_REASON';
    }else{
      query.property='o.ORDER_DATE';
    }
/* *direction*/
if(sort[1] !=undefined && (sort[1].toUpperCase()=='DESC' || sort[1].toUpperCase()=='ASC')){
  query.direction=sort[1].toUpperCase();  
}else{
    query.direction='DESC';
}
logger.info(`Order.service : mapRejectedPageSorting : Ended`);
return query;
};

function mapPageSorting( query ) {
  logger.info(`Order.service : mapPageSorting : Started`);

  sort=query.sort.split(",");
    /* property*/
    if(sort[0]=='1'){
      query.property='o.ORDER_NUMBER';
    }else if(sort[0]=='2'){
      query.property='o.ORDER_DATE';
    }else if(sort[0]=='3') {
      if(query.agentId!= undefined){
        query.property='l.NAME_ARABIC';
      }else{
          query.property='l.NAME_ENGLISH';
      }
    }else{
      query.property='o.ORDER_DATE';
    }
/* *direction*/
if(sort[1] !=undefined && (sort[1].toUpperCase()=='DESC' || sort[1].toUpperCase()=='ASC')){
  query.direction=sort[1].toUpperCase();  
}else{
    query.direction='DESC';
}
logger.info(`Order.service : mapPageSorting : Ended`);
return query;
};

exports.openOrder =(query) => {
  return new Promise(async (resolve, reject) => {
  try{
    let orders= await orderDao.getOrdersByOrderNumber(query);
    if(orders.length !=0){
      let order_id=orders[0].ORDER_ID;
      let products= await orderDao.getProductByOrderId({ORDER_ID:order_id}); 
    
      if(orders[0].STATUS_ID == BusinessConstants.ORDER_STATUS_APPROVED1){
      for(var i = 0; i < products.length;i++){
          query.orderId = orders[0].ORDER_ID;
          query.productId = products[i].PRODUCT_ID;
          let approvedQuantity= orderDao.getApprovedQuantity(query);
          products[0].approvedQuantity=approvedQuantity;
        }
     }
      orders[0].products=products;
    }
    logger.info(`Order.service : openOrder : Ended`);
    resolve(orders); 
  }
  catch(err){
    if(err.operationName ==undefined){
        err.operationName='Order.service';
        err.serviceName='openOrder';
     } 
     reject(err)     
  }  
});
}


