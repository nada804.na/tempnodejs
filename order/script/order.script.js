module.exports.findOrdersByStatusIdAndAgentIdScript=(bind) =>{
  
  let script=`SELECT o.ORDER_NUMBER as orderNumber ,o.ORDER_DATE as orderDate,
              o.REJECTION_REASON as rejectionReason ,
              a.NAME_ARABIC as agent_nameArabic ,a.NAME_ENGLISH as agent_nameEnglish,
              l.NAME_ARABIC as orderReasonLookup_nameArabic
              FROM DCA_ORDER o,DCA_AGENT a,DCA_SYSTEM_LOOKUPS l
              WHERE a.AGENT_ID=o.AGENT_ID and o.REASON_CODE_ID =l.LOOKUP_ID 
              AND l.LOOKUP_TYPE_ID =14
              AND o.STATUS_ID in (${bind.statusId}) `;
                   if(bind.agentId !=undefined){
                      script +=`AND o.AGENT_ID = ${bind.agentId} `;
                   }
                   script +=`ORDER BY  ${bind.property} ${bind.direction}`;
                  
  return script;            
};

module.exports.findByStatusIdAndAgentIdPageScript=(bind) =>{
  let script=`SELECT outer.*
              FROM (SELECT ROWNUM rn, inner.*
                FROM (SELECT * 
                      FROM DCA_ORDER o INNER JOIN DCA_AGENT a 
                        ON a.AGENT_ID=o.AGENT_ID    
                        WHERE o.STATUS_ID = ${bind.statusId} AND o.AGENT_ID = ${bind.agentId}
                        ORDER BY  ${bind.orderBy} ${ordering}
                        ) inner) outer
              WHERE outer.rn >= ${bind.offest} AND outer.rn <= ${bind.limit}`;
  return script;            
};


module.exports.getOrdersByOrderNumber=(bind) =>{
  
  let script=`SELECT o.ORDER_ID ,o.STATUS_ID ,o.ORDER_NUMBER as orderNumber ,
              o.ORDER_DATE as orderDate,
              l.NAME_ARABIC as orderReasonLookup_nameArabic
              FROM DCA_ORDER o , DCA_SYSTEM_LOOKUPS l
              WHERE  o.REASON_CODE_ID =l.LOOKUP_ID and
              o.ORDER_NUMBER = ${bind.orderNumber} AND l.LOOKUP_TYPE_ID =14`; 

  return script;            
};

module.exports.getProductByOrderId=(bind) =>{
  
  let script=`SELECT p.PRODUCT_ID as PRODUCT_ID ,p.name_english as product_nameEnglish
              ,p.name_arabic as product_nameArabic,
               order_pro.QUANTITY as quantity
              FROM DCA_ORDER_PRODUCTS order_pro INNER JOIN  DCA_ORDER o
              ON order_pro.ORDER_ID=o.ORDER_ID INNER JOIN DCA_PRODUCTS p
              ON  order_pro.PRODUCT_ID=p.PRODUCT_ID 
              WHERE o.ORDER_ID = ${bind.ORDER_ID}`; 
  return script;            
};

/* 
module.exports.openOrder=(bind) =>{
  
  let script=`SELECT o.ORDER_NUMBER as orderNumber ,
              o.ORDER_DATE as orderDate,
              o.REJECTION_REASON as rejectionReason ,
              l.NAME_ARABIC as orderReasonLookup_nameArabic
              , p.name_english  as product_nameEnglish
              ,p.name_arabic as product_nameArabic,
              order_pro.QUANTITY as quantity
              FROM DCA_ORDER_PRODUCTS order_pro INNER JOIN  DCA_ORDER o
              ON order_pro.ORDER_ID=o.ORDER_ID INNER JOIN DCA_PRODUCTS p
              ON  order_pro.PRODUCT_ID=p.PRODUCT_ID inner join DCA_SYSTEM_LOOKUPS l
              on  o.REASON_CODE_ID =l.LOOKUP_ID
              WHERE o.ORDER_NUMBER = ${bind.orderNumber} AND l.LOOKUP_TYPE_ID =14`; 

  return script;            
};
 */
module.exports.getApprovedQuantity=(bind) =>{  
  let script=`select p.QUANTITY as quantityApproved
              from DCA_TRANSACTIONS t left outer join 
              DCA_TRANSACTION_PRODUCTS p on t.TRANSACTION_ID=p.TRANSACTION_ID 
              where t.ORDER_ID= ${bind.orderId} and p.PRODUCT_ID=${bind.productId}`; 

  return script;            
};

module.exports.addOrder=(bind) =>{
  /* loginId,AgentId,OrderDate*/
  let script=`INSERT INTO table
                (column1, column2, ... column_n )
                VALUES
                (expression1, expression2, ... expression_n )`;
                                
  return script;            
};



module.exports.findByAllUsersPage=(statusId,agentId,offest,limit,orderBy,ordering) =>{
  let script=`SELECT outer.*
              FROM (SELECT ROWNUM rn, inner.*
                FROM (SELECT o.*  FROM users o  ORDER BY  ${orderBy} ${ordering}) inner
                ) outer WHERE outer.rn >= ${offest} AND outer.rn <= ${limit}`;
  return script;            
};